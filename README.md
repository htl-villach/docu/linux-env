# Arbeitsumgebung

## Umgebung einrichten

* _Terminal_ öffnen
* Kommando `setupenv` eingeben
* Es öffnet sich ein Browserfenster für das Login
* Anmeldung mit dem eigenen Microsoft Account `schueler@edu.htl-villach.at`
* Nach erfolgreicher Anmeldung erscheint wird das Kommando im _Terminal_ fortgesetzt 
* Das _Terminal_  und der Browser können dann geschlossen werden

## Persönliches Repository klonen

Eine der folgenden URLs kopieren und `NACHNAME` durch den eigenen Nachnamen (ohne Sonderzeichen etc.) ersetzen.

### 2AHIF

```text
https://gitlab.com/htl-villach/informatik/2024-2ahif/pos/NACHNAME.git
```
```text
https://gitlab.com/htl-villach/informatik/2024-2ahif/wmc/NACHNAME.git
```

### 2BHIF

```text
https://gitlab.com/htl-villach/informatik/2024-2bhif/pos/NACHNAME.git
```
```text
https://gitlab.com/htl-villach/informatik/2024-2bhif/wmc/NACHNAME.git
```

### 3AHIF

```text
https://gitlab.com/htl-villach/informatik/2024-3ahif/pos/NACHNAME.git
```
```text
https://gitlab.com/htl-villach/informatik/2024-3ahif/wmc/NACHNAME.git
```

### 3BHIF

```text
https://gitlab.com/htl-villach/informatik/2024-3bhif/nscs/NACHNAME.git
```
```text
https://gitlab.com/htl-villach/informatik/2024-3bhif/wmc/NACHNAME.git
```

### 4AHIF
```text
https://gitlab.com/htl-villach/informatik/2024-4ahif/cda/NACHNAME.git
```
```text
https://gitlab.com/htl-villach/informatik/2024-4ahif/dbi/NACHNAME.git
```
```text
https://gitlab.com/htl-villach/informatik/2024-4ahif/pos/NACHNAME.git
```
```text
https://gitlab.com/htl-villach/informatik/2024-4ahif/syp/NACHNAME.git
```
```text
https://gitlab.com/htl-villach/informatik/2024-4ahif/wmc/NACHNAME.git
```

### 4BHIF

```text
https://gitlab.com/htl-villach/informatik/2024-4ahif/cda/NACHNAME.git
```
```text
https://gitlab.com/htl-villach/informatik/2024-4bhif/syp/NACHNAME.git
```
```text
https://gitlab.com/htl-villach/informatik/2024-4bhif/wmc/NACHNAME.git
```

### 5AHIF

```text
https://gitlab.com/htl-villach/informatik/2024-5ahif/cda/NACHNAME.git
```
```text
https://gitlab.com/htl-villach/informatik/2024-5ahif/syp/NACHNAME.git
```
```text
https://gitlab.com/htl-villach/informatik/2024-5ahif/wmc/NACHNAME.git
```

### 5BHIF

```text
https://gitlab.com/htl-villach/informatik/2024-5ahif/cda/NACHNAME.git
```
```text
https://gitlab.com/htl-villach/informatik/2024-5bhif/syp/NACHNAME.git
```
```text
https://gitlab.com/htl-villach/informatik/2024-5bhif/wmc/NACHNAME.git
```

